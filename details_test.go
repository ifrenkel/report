package report

import (
	"testing"

	"github.com/stretchr/testify/require"
)

type marshallable interface {
	MarshalJSON() ([]byte, error)
}

func TestDetailsMarshalJSON(t *testing.T) {
	cases := []struct {
		name     string
		obj      marshallable
		expected string
	}{
		{
			"text field",
			DetailsTextField{Name: "foo", Value: "bar"},
			`{"type":"text","name":"foo","value":"bar"}`,
		},
		{
			"url field",
			DetailsURLField{Text: "foo", Name: "foo", Href: "test.com"},
			`{"type":"url","name":"foo","text":"foo","href":"test.com"}`,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			actual, err := c.obj.MarshalJSON()
			require.NoError(t, err)
			require.JSONEq(t, c.expected, string(actual))
		})
	}
}
